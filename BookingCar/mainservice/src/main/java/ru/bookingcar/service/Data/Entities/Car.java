package ru.bookingcar.service.Data.Entities;

import org.springframework.validation.annotation.Validated;
import ru.bookingcar.service.Data.Enums.Brand;
import ru.bookingcar.service.Data.Enums.TypeCar;
import ru.bookingcar.service.controllers.DTO.Request.CarCreationDTO;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.UUID;

@Entity
@Validated
@Table(name = "car")
public class Car {

    @Id
    @Column(name = "id")
    private UUID carId;
    @Valid
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "type_car")
    private TypeCar typeCar;
    @Min(0)
    @Column(name = "trips_count")
    private Integer tripsCount;
    @Enumerated(EnumType.STRING)
    @Column(name = "brand")
    private Brand brand;
    @NotBlank
    @Column(name = "name")
    private String name;
    @Min(0)
    @Max(5)
    @Column(name = "rating")
    private float rating;
    @Column(name = "base_price")
    private int basePrice;

    public Car(UUID carId, @Valid TypeCar typeCar, @Min(0) Integer tripsCount, @Valid Brand brand, @NotBlank String name, float rating, int basePrice) {
        this.carId = carId;
        this.typeCar = typeCar;
        this.tripsCount = tripsCount;
        this.brand = brand;
        this.name = name;
        this.rating = rating;
        this.basePrice = basePrice;
    }

    public Car() {
    }

    public UUID getCarId() {
        return carId;
    }

    public void setCarId(UUID carId) {
        this.carId = carId;
    }

    public TypeCar getTypeCar() {
        return typeCar;
    }

    public void setTypeCar(TypeCar typeCar) {
        this.typeCar = typeCar;
    }

    public Integer getTripsCount() {
        return tripsCount;
    }

    public void setTripsCount(Integer tripsCount) {
        this.tripsCount = tripsCount;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(int basePrice) {
        this.basePrice = basePrice;
    }

    public Integer getCost(){
        return Math.round(getBasePrice() * (getRating() + 1));
    }

}
