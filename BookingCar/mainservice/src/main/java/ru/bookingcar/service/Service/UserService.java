package ru.bookingcar.service.Service;

import ru.bookingcar.service.Exeptions.CarNotFoundException;
import ru.bookingcar.service.Exeptions.UserNotFoundException;
import ru.bookingcar.service.controllers.DTO.Request.NewUserDTO;
import ru.bookingcar.service.controllers.DTO.Response.UserDTO;

import java.util.List;
import java.util.UUID;

public interface UserService {

    UserDTO addNewUser(NewUserDTO user);

    List<UserDTO> findAllUsers();

    UserDTO findUserByEmail(String email) throws UserNotFoundException;

    void deleteUser(UUID id)throws UserNotFoundException;

}
