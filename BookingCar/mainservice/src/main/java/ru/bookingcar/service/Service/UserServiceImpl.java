package ru.bookingcar.service.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bookingcar.service.Data.Entities.User;
import ru.bookingcar.service.Data.Repository.UsersRepository;
import ru.bookingcar.service.Exeptions.BookingNotFoundException;
import ru.bookingcar.service.Exeptions.UserNotFoundException;
import ru.bookingcar.service.Mappers.UserMapper;
import ru.bookingcar.service.controllers.DTO.Request.NewUserDTO;
import ru.bookingcar.service.controllers.DTO.Response.UserDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service("userServiceImpl")
public class UserServiceImpl implements UserService {


    private final UsersRepository repository;
    private final UserMapper userMapper;

    @Autowired
    public UserServiceImpl(UsersRepository repository, UserMapper userMapper) {
        this.repository = repository;
        this.userMapper = userMapper;
    }


    @Override
    public UserDTO addNewUser(NewUserDTO user) {
        User newUser = userMapper.convertFromDTO(user);
        repository.save(newUser);
        return userMapper.convertFromEntity(newUser);
    }

    @Override
    public List<UserDTO> findAllUsers() {
        ArrayList<UserDTO> result = new ArrayList<>();
        repository.findAll().forEach(item -> {
            result.add(userMapper.convertFromEntity(item));
        });
        return result;
    }

    @Override
    public UserDTO findUserByEmail(String email) throws UserNotFoundException {
        return userMapper.convertFromEntity(
                repository.findUserByEmail(email).orElseThrow(()->
                        new UserNotFoundException("User with this email doesn't exist")));
    }

    @Override
    public void deleteUser(UUID id) {
        repository.deleteById(id);
    }
}
