package ru.bookingcar.service.Data.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bookingcar.service.Data.Entities.Car;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface CarsRepository extends JpaRepository<Car, UUID> {

    Optional<Car> findByCarId(UUID uuid);

    void deleteByCarId(UUID uuid);

    List<Car> findAllByBrand(String brand);

}
