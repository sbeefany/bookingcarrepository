package ru.bookingcar.service.Exeptions;

public class CarNotFoundException extends Exception{

    public CarNotFoundException(String message) {
        super(message);
    }
}
