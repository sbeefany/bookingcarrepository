package ru.bookingcar.service.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.bookingcar.service.Exeptions.CarNotFoundException;
import ru.bookingcar.service.Exeptions.UserNotFoundException;
import ru.bookingcar.service.Service.BookingService;
import ru.bookingcar.service.controllers.DTO.Request.BookingCreationDTO;
import ru.bookingcar.service.controllers.DTO.Response.BookingDTO;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/service/bookings")
@Api("Api для работы с бронированием")
public class BookingController {

    private final BookingService bookingService;

    @Autowired
    public BookingController(@Qualifier("bookingServiceImpl") BookingService bookingService) {
        this.bookingService = bookingService;
    }


    @PostMapping("/newBooking")
    @ApiOperation("Метод для создания брони, требуются даты брони, а также Id пользователя и машины")
    public ResponseEntity<BookingDTO> createBooking(@RequestBody BookingCreationDTO creationDTO) {
        try {
            return ResponseEntity.ok(bookingService.createBooking(creationDTO));
        } catch (UserNotFoundException | CarNotFoundException | IllegalStateException | ParseException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping("/")
    @ApiOperation("Метод для поиска все[ бронирований на заданные даты. Метод расчитывается для работы администратора")
    public ResponseEntity<List<BookingDTO>> findBookings(@RequestParam(required = false)  @NotBlank String dateFrom,
                                                         @RequestParam(required = false)  @NotBlank String dateTo) {
        try {
            return ResponseEntity.ok(bookingService.getAllBookings(dateFrom, dateTo));
        } catch (ParseException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}")
    @ApiOperation("Метод для поиска бронирования по id")
    public ResponseEntity<BookingDTO> findBooking(@PathVariable("id") UUID bookingId) {
        return null;
    }

    @GetMapping("/users/{userId}")
    @ApiOperation("Метод для поиска бронирований по id пользователя")
    public ResponseEntity<List<BookingDTO>> findBookingByUserId(@PathVariable("userId") UUID userId) {
        try {
            return ResponseEntity.ok(bookingService.findBookingsByUserId(userId));
        } catch (UserNotFoundException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping("/cars/{carId}")
    @ApiOperation("Метод для поиска бронирований по id машины")
    public ResponseEntity<List<BookingDTO>> findBookingByCarId(@PathVariable("carId") UUID carId) {
        try {
            return ResponseEntity.ok(bookingService.findBookingsByCarId(carId));
        } catch (CarNotFoundException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


}
