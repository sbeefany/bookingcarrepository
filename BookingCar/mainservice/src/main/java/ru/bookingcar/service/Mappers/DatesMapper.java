package ru.bookingcar.service.Mappers;

import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class DatesMapper {

    public Date convertFromString(String dateString) throws ParseException {
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dateString);
        return date;
    }
}
