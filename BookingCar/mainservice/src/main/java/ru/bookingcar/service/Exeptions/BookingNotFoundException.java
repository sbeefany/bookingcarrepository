package ru.bookingcar.service.Exeptions;

public class BookingNotFoundException extends Exception{

    public BookingNotFoundException(String message) {
        super(message);
    }
}
