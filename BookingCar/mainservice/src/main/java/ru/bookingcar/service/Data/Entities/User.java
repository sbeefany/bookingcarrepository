package ru.bookingcar.service.Data.Entities;

import org.hibernate.validator.constraints.UniqueElements;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.validation.annotation.Validated;
import ru.bookingcar.service.controllers.DTO.Request.NewUserDTO;
import ru.bookingcar.service.controllers.DTO.Response.UserDTO;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "user_entity")
@Validated
public class User {

    @Id
    @Column(name = "id")
    private UUID id;

    @NotBlank
    @Column(name = "email")
    private String email;

    @NotBlank
    @Column(name = "first_name")
    private String firstName;

    @NotBlank
    @Column(name = "second_name")
    private String secondName;

    public User(@NotBlank UUID id, @NotBlank String email,
                @NotBlank String firstName, @NotBlank String secondName) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.secondName = secondName;
    }

    public User() {

    }
    
    @NonNull
    public UUID getId() {
        return id;
    }

    public void setId(@NonNull UUID id) {
        this.id = id;
    }

    @NonNull
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(@NonNull String firstName) {
        this.firstName = firstName;
    }

    @NonNull
    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(@NonNull String secondName) {
        this.secondName = secondName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
