package ru.bookingcar.service.Data.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.bookingcar.service.Data.Entities.User;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UsersRepository extends CrudRepository<User, UUID> {

    Optional<User> findUserById(UUID id);

    Optional<User> findUserByEmail(String email);


}
