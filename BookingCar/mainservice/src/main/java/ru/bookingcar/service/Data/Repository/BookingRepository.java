package ru.bookingcar.service.Data.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.bookingcar.service.Data.Entities.Booking;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository
public interface BookingRepository extends JpaRepository<Booking, UUID> {

    @Query(value = "select * from Booking where (date_to>=?1 and date_to<= ?2) or (date_from>=?1 and date_from<=?2) or (date_from<= ?1 and date_to>= ?2)",
    nativeQuery = true)
    List<Booking> findBookingsWithDates(Date dateFrom,Date dateTo);

}
