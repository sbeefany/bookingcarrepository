package ru.bookingcar.service.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bookingcar.service.Data.Entities.Car;
import ru.bookingcar.service.Data.Repository.CarsRepository;
import ru.bookingcar.service.Exeptions.CarNotFoundException;
import ru.bookingcar.service.Mappers.CarMapper;
import ru.bookingcar.service.controllers.DTO.Request.CarCreationDTO;
import ru.bookingcar.service.controllers.DTO.Response.BookingDTO;
import ru.bookingcar.service.controllers.DTO.Response.CarDTO;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CarServiceImpl implements CarService {

    private final CarsRepository carsRepository;
    private final BookingService bookingService;
    private final CarMapper carMapper;

    @Autowired
    public CarServiceImpl(CarsRepository carsRepository, BookingService bookingService, CarMapper carMapper) {
        this.carsRepository = carsRepository;
        this.bookingService = bookingService;
        this.carMapper = carMapper;
    }

    @Override
    public CarDTO addNewCarToPark(CarCreationDTO car) {
        Car newCar = carMapper.convertFromDTO(car);
        carsRepository.save(newCar);
        return carMapper.convertFromEntity(newCar);
    }

    @Override
    public void deleteCar(UUID carId) throws CarNotFoundException {
        carsRepository.deleteByCarId(carId);
    }

    @Override
    public CarDTO findCar(UUID carId) throws CarNotFoundException {
        return carMapper.convertFromEntity(carsRepository.findByCarId(carId).orElseThrow());
    }

    @Override
    public List<CarDTO> getAllCars() {
        return carsRepository.findAll().stream().map(carMapper::convertFromEntity).collect(Collectors.toList());
    }

    @Override
    public List<CarDTO> findFreeCars(String dateFrom, String dateTo) throws CarNotFoundException, ParseException {
        List<Car> cars = new ArrayList<>(carsRepository.findAll());
        List<BookingDTO> bookingDTOS = bookingService.getAllBookings(dateFrom, dateTo);
        return cars.stream()
                .filter(car ->
                        bookingDTOS.stream().noneMatch(bookingDTO -> bookingDTO.getCarId().equals(car.getCarId())))
                .map(carMapper::convertFromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public void sendCarToTechnicalInspection(UUID carId) throws CarNotFoundException {

    }
}
