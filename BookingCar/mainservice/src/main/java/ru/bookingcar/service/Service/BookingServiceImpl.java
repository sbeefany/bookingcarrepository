package ru.bookingcar.service.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bookingcar.service.Data.Entities.Booking;
import ru.bookingcar.service.Data.Enums.Status;
import ru.bookingcar.service.Data.Repository.BookingRepository;
import ru.bookingcar.service.Exeptions.BookingNotFoundException;
import ru.bookingcar.service.Exeptions.CarNotFoundException;
import ru.bookingcar.service.Exeptions.UserNotFoundException;
import ru.bookingcar.service.Mappers.BookingMapper;
import ru.bookingcar.service.Mappers.DatesMapper;
import ru.bookingcar.service.controllers.DTO.Request.BookingCreationDTO;
import ru.bookingcar.service.controllers.DTO.Response.BookingDTO;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service("bookingServiceImpl")
public class BookingServiceImpl implements BookingService {

    private final BookingRepository bookingRepository;
    private final BookingMapper bookingMapper;
    private final DatesMapper datesMapper;
    private final int baseCost = 1000;


    @Autowired
    public BookingServiceImpl(BookingRepository bookingRepository, BookingMapper bookingMapper, DatesMapper datesMapper) {
        this.bookingRepository = bookingRepository;
        this.bookingMapper = bookingMapper;
        this.datesMapper = datesMapper;
    }

    @Override
    public BookingDTO createBooking(BookingCreationDTO bookingCreationDTO) throws UserNotFoundException, CarNotFoundException, IllegalStateException, ParseException {
        if (checkStatusCar(bookingCreationDTO.getDateFrom(),bookingCreationDTO.getDateTo(),bookingCreationDTO.getCarId())) {
            Booking booking = bookingMapper.convertFromCreationDTO(bookingCreationDTO);
            bookingRepository.save(booking);
            return bookingMapper.convertFromEntity(booking);
        } else {
            throw new IllegalStateException("Booking with these dates and this car has already been");
        }

    }

    @Override
    public void changeStatus(String status, UUID uuid) throws BookingNotFoundException {
        Booking booking = bookingRepository.findById(uuid).orElseThrow(() -> new BookingNotFoundException("Booking with this id doesn't exist"));
        if (!booking.getStatus().equals(Status.valueOf(status))) {
            booking.setStatus(Status.valueOf(status));
            bookingRepository.save(booking);
        }

    }

    @Override
    public List<BookingDTO> getAllBookings(String dateFrom, String dateTo) throws ParseException {
        if (dateFrom == null || dateTo == null || dateFrom.isEmpty() || dateTo.isEmpty() )
            return bookingRepository.findAll().stream().map(bookingMapper::convertFromEntity).collect(Collectors.toList());
        else {
            return bookingRepository.findBookingsWithDates(datesMapper.convertFromString(dateFrom), datesMapper.convertFromString(dateTo)).stream().map(bookingMapper::convertFromEntity).collect(Collectors.toList());
        }
    }

    @Override
    public BookingDTO findBookingById(UUID bookingId) throws BookingNotFoundException {
        return bookingMapper.convertFromEntity(bookingRepository.findById(bookingId).orElseThrow(
                () -> new BookingNotFoundException("Booking with this id doesn't exist")));
    }

    @Override
    public List<BookingDTO> findBookingsByUserId(UUID userId) {
        return bookingRepository.findAll().stream()
                .filter(booking -> booking.getUser().getId().equals(userId))
                .map(bookingMapper::convertFromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public List<BookingDTO> findBookingsByCarId(UUID carId) {
        return bookingRepository.findAll().stream()
                .filter(booking -> booking.getCar().getCarId().equals(carId))
                .map(bookingMapper::convertFromEntity)
                .collect(Collectors.toList());
    }

    public boolean checkStatusCar(String dateFrom,String dateTo,UUID carId) throws ParseException {
        List<Booking> bookingDTOS = bookingRepository.findBookingsWithDates(datesMapper.convertFromString(dateFrom), datesMapper.convertFromString(dateTo));
        return bookingDTOS
                .stream()
                .noneMatch(bookingDTO ->
                        bookingDTO.getCar().getCarId().equals(carId));
    }

}
