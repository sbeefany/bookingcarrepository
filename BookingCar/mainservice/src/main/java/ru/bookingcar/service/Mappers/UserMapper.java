package ru.bookingcar.service.Mappers;

import org.springframework.stereotype.Component;
import ru.bookingcar.service.Data.Entities.User;
import ru.bookingcar.service.controllers.DTO.Request.NewUserDTO;
import ru.bookingcar.service.controllers.DTO.Response.UserDTO;

import java.util.UUID;

@Component
public class UserMapper {

    public User convertFromDTO(NewUserDTO userDTO){
        return new User(UUID.randomUUID(), userDTO.getEmail(), userDTO.getFirstName(), userDTO.getSecondName());
    }

    public UserDTO convertFromEntity(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setEmail(user.getEmail());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setSecondName(user.getSecondName());
        return userDTO;
    }
}
