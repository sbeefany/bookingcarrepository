package ru.bookingcar.service.controllers.DTO.Response;

import org.springframework.validation.annotation.Validated;
import ru.bookingcar.service.Data.Entities.Car;
import ru.bookingcar.service.Data.Enums.Brand;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.UUID;

@Validated
public class CarDTO {


    @NotBlank
    private UUID id;

    @NotBlank
    private String typeCar;

    @NotBlank
    private String name;

    @NotBlank
    private Brand brand;

    @Min(0)
    @Max(5)
    private float rating;


    public String getTypeCar() {
        return typeCar;
    }

    public void setTypeCar(String typeCar) {
        this.typeCar = typeCar;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }


}
