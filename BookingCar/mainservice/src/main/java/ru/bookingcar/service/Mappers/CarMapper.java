package ru.bookingcar.service.Mappers;

import org.springframework.stereotype.Component;
import ru.bookingcar.service.Data.Entities.Car;
import ru.bookingcar.service.Data.Enums.Brand;
import ru.bookingcar.service.Data.Enums.TypeCar;
import ru.bookingcar.service.controllers.DTO.Request.CarCreationDTO;
import ru.bookingcar.service.controllers.DTO.Response.CarDTO;

import java.util.UUID;

@Component
public class CarMapper {

    public CarDTO convertFromEntity(Car car) {
        CarDTO carDTO = new CarDTO();
        carDTO.setId(car.getCarId());
        carDTO.setBrand(car.getBrand());
        carDTO.setTypeCar(car.getTypeCar().name());
        carDTO.setName(car.getName());
        carDTO.setRating(car.getRating());
        return carDTO;
    }

    public Car convertFromDTO(CarCreationDTO carDTO) {
        return new Car(UUID.randomUUID(), TypeCar.valueOf(carDTO.getTypeCar()), carDTO.getTripsCount(),
                Brand.valueOf(carDTO.getBrand()), carDTO.getName(), 2.5f, carDTO.getBasePrice());
    }
}
