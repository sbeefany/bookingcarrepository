package ru.bookingcar.service.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.bookingcar.service.Exeptions.UserNotFoundException;
import ru.bookingcar.service.Service.UserService;
import ru.bookingcar.service.controllers.DTO.Request.NewUserDTO;
import ru.bookingcar.service.controllers.DTO.Response.UserDTO;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/service/users")
@Api("Api для работы с пользователями")
public class UsersController {

    private final UserService userService;

    @Autowired
    public UsersController(@Qualifier("userServiceImpl") UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/")
    @ApiOperation("Метод для для поиска всех пользователей, предназначен для работы Администратора")
    public ResponseEntity<List<UserDTO>> findAllUsers() {
        return ResponseEntity.ok(userService.findAllUsers());
    }

    @PostMapping("/")
    @ApiOperation("Метод для для добавления пользователя, будет вызываться при регистрации")
    public ResponseEntity<UserDTO> addNewUser(@RequestBody NewUserDTO userDTO) {
        try {
            return ResponseEntity.ok(userService.addNewUser(userDTO));
        }catch (Exception e){
            return new ResponseEntity(e.getMessage(),HttpStatus.BAD_REQUEST);
        }

    }

    @DeleteMapping("/{id}")
    @ApiOperation("Метод для для удаления пользователя, будет вызываться при блокировании пользователя")
    public ResponseEntity<?> deleteUser(@PathVariable("id") UUID userId)  {
        try {
            userService.deleteUser(userId);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{email}")
    @ApiOperation("Метод для для поиска пользователя по Id")
    public ResponseEntity<UserDTO> getUser(@PathVariable("email") String email)  {
        try {
            return ResponseEntity.ok(userService.findUserByEmail(email));
        } catch (UserNotFoundException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

}
