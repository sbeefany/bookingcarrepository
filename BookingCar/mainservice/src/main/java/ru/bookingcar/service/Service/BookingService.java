package ru.bookingcar.service.Service;


import ru.bookingcar.service.Exeptions.BookingNotFoundException;
import ru.bookingcar.service.Exeptions.CarNotFoundException;
import ru.bookingcar.service.Exeptions.UserNotFoundException;
import ru.bookingcar.service.controllers.DTO.Request.BookingCreationDTO;
import ru.bookingcar.service.controllers.DTO.Response.BookingDTO;
import ru.bookingcar.service.controllers.DTO.Response.CarDTO;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface BookingService {

    BookingDTO createBooking(BookingCreationDTO bookingCreationDTO) throws UserNotFoundException, CarNotFoundException, ParseException;

    void changeStatus(String status,UUID uuid) throws BookingNotFoundException;

    List<BookingDTO> getAllBookings(String dateFrom,String dateTo) throws ParseException;

    BookingDTO findBookingById(UUID bookingId) throws BookingNotFoundException;

    List<BookingDTO> findBookingsByUserId(UUID userId) throws UserNotFoundException;

    List<BookingDTO> findBookingsByCarId(UUID carId) throws CarNotFoundException;

}
