package ru.bookingcar.service.controllers.DTO.Response;

import org.springframework.validation.annotation.Validated;
import ru.bookingcar.service.Data.Entities.User;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
@Validated
public class UserDTO {

    @NotBlank
    private UUID id;
    @NotBlank
    private String email;

    @NotBlank
    private String firstName;

    @NotBlank
    private String secondName;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstname) {
        this.firstName = firstname;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }


}
