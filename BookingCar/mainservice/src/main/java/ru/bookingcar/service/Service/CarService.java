package ru.bookingcar.service.Service;


import ru.bookingcar.service.Exeptions.CarNotFoundException;
import ru.bookingcar.service.controllers.DTO.Request.CarCreationDTO;
import ru.bookingcar.service.controllers.DTO.Response.CarDTO;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface CarService {

    CarDTO addNewCarToPark(CarCreationDTO car);

    void deleteCar(UUID carId) throws CarNotFoundException;

    CarDTO findCar(UUID carId) throws CarNotFoundException;;

    List<CarDTO> getAllCars();

    List<CarDTO> findFreeCars(String  dateFrom,String  dateTo) throws CarNotFoundException, ParseException;

    void sendCarToTechnicalInspection(UUID carId) throws CarNotFoundException;;
}
