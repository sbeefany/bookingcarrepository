package ru.bookingcar.service.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.bookingcar.service.Exeptions.CarNotFoundException;
import ru.bookingcar.service.Service.CarServiceImpl;
import ru.bookingcar.service.controllers.DTO.Request.CarCreationDTO;
import ru.bookingcar.service.controllers.DTO.Response.CarDTO;

import javax.validation.Valid;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/service/cars")
@Api("Api для работы с машинами")
public class CarsController {

    private final CarServiceImpl carService;

    @Autowired
    public CarsController(CarServiceImpl carService) {
        this.carService = carService;
    }

    @PostMapping("/")
    @ApiOperation("Метод для для добавления машины в автопарк, преднозначен для Администратора")
    public ResponseEntity<CarDTO> addNewCarToPark(@RequestBody @Valid CarCreationDTO carDto) {
        return ResponseEntity.ok(carService.addNewCarToPark(carDto));
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Метод для для удаления машины из автопарка, преднозначен для Администратора")
    public ResponseEntity<?> deleteCar(@PathVariable("id") @NotBlank UUID carId) {
        try {
            carService.deleteCar(carId);
           return new ResponseEntity<>(HttpStatus.OK);
        } catch (CarNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}")
    @ApiOperation("Метод для для поиска машины по id")
    public ResponseEntity<CarDTO> findCar(@PathVariable("id") @NotBlank UUID carId) {
        try {
            return ResponseEntity.ok(carService.findCar(carId));
        } catch (CarNotFoundException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/freeCars")
    @ApiOperation("Метод для для поиска свободных машин на заданные числа")
    public ResponseEntity<List<CarDTO>> findFreeCars(@RequestParam(required = false) @Future String dateFrom,
                                     @RequestParam(required = false) @Future String dateTo,
                                     @RequestParam(required = false) String brand,
                                     @RequestParam(required = false) Float rating) {
        try {
            return ResponseEntity.ok(carService.findFreeCars(dateFrom,dateTo));
        } catch (CarNotFoundException | ParseException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/")
    @ApiOperation("Метод для получения всех машин, предназначено для Администратора")
    public ResponseEntity<List<CarDTO>> getAllCars() {
        return ResponseEntity.ok(carService.getAllCars());
    }

    @PostMapping("/{id}/technicalInspection")
    @ApiOperation("Метод для отправки машины на ТО")
    public ResponseEntity<?> sendCarToTechnicalInspection(@NotBlank UUID carId) {
        try {
            carService.sendCarToTechnicalInspection(carId);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (CarNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

}
