package ru.bookingcar.service.Mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bookingcar.service.Data.Entities.Booking;
import ru.bookingcar.service.Data.Entities.Car;
import ru.bookingcar.service.Data.Entities.User;
import ru.bookingcar.service.Data.Enums.Status;
import ru.bookingcar.service.Data.Repository.CarsRepository;
import ru.bookingcar.service.Data.Repository.UsersRepository;
import ru.bookingcar.service.Exeptions.CarNotFoundException;
import ru.bookingcar.service.Exeptions.UserNotFoundException;
import ru.bookingcar.service.controllers.DTO.Request.BookingCreationDTO;
import ru.bookingcar.service.controllers.DTO.Response.BookingDTO;

import java.text.ParseException;
import java.util.Date;
import java.util.UUID;

@Component
public class BookingMapper {

    private final UsersRepository usersRepository;
    private final CarsRepository carsRepository;
    private final DatesMapper datesMapper;

    @Autowired
    public BookingMapper(UsersRepository usersRepository, CarsRepository carsRepository, DatesMapper datesMapper) {
        this.usersRepository = usersRepository;
        this.carsRepository = carsRepository;
        this.datesMapper = datesMapper;
    }

    public Booking convertFromCreationDTO(BookingCreationDTO bookingCreationDTO) throws UserNotFoundException, CarNotFoundException, ParseException {
        Date dateFrom = datesMapper.convertFromString(bookingCreationDTO.getDateFrom());
        Date dateTo = datesMapper.convertFromString(bookingCreationDTO.getDateTo());
        User user = usersRepository.findUserById(bookingCreationDTO.getUserId()).orElseThrow(() -> new UserNotFoundException("User with this id doesn't exist"));
        Car car = carsRepository.findByCarId(bookingCreationDTO.getCarId()).orElseThrow(() -> new CarNotFoundException("User with this id doesn't exist"));
        return new Booking(UUID.randomUUID(), user,
               dateFrom, dateTo , car, Long.valueOf(car.getCost() * (dateTo.getTime() - dateFrom.getTime()) / 1000 / 60 / 60 / 24 / 28).doubleValue(), Status.Open);
    }

    public Booking convertFromDTO(BookingDTO bookingDTO) throws UserNotFoundException, CarNotFoundException {
        User user = usersRepository.findUserById(bookingDTO.getUserId()).orElseThrow(() -> new UserNotFoundException("User with this id doesn't exist"));
        Car car = carsRepository.findByCarId(bookingDTO.getCarId()).orElseThrow(() -> new CarNotFoundException("User with this id doesn't exist"));
        return new Booking(UUID.randomUUID(), user,
                bookingDTO.getDateFrom(), bookingDTO.getDateTo(), car, bookingDTO.getCost(), bookingDTO.getStatus());
    }

    public BookingDTO convertFromEntity(Booking booking){
        BookingDTO bookingDTO = new BookingDTO();
        bookingDTO.setBookingId(booking.getBookingId());
        bookingDTO.setCost(booking.getCost());
        bookingDTO.setDateFrom(booking.getDateFrom());
        bookingDTO.setDateTo(booking.getDateTo());
        bookingDTO.setStatus(booking.getStatus());
        bookingDTO.setUserId(booking.getUser().getId());
        bookingDTO.setCarId(booking.getCar().getCarId());
        return bookingDTO;
    }
}
