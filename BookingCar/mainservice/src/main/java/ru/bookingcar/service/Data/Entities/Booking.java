package ru.bookingcar.service.Data.Entities;

import org.springframework.validation.annotation.Validated;
import ru.bookingcar.service.Data.Enums.Status;
import ru.bookingcar.service.controllers.DTO.Request.BookingCreationDTO;
import ru.bookingcar.service.controllers.DTO.Response.BookingDTO;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Future;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.UUID;

@Entity
@Validated
@Table(name = "booking")
public class Booking {

    @Id
    @Column(name = "id")
    private UUID bookingId;
    @ManyToOne
    @Valid
    @JoinColumn(referencedColumnName = "id")
    private User user;
    @Future
    @Column(name = "date_from")
    private Date dateFrom;
    @Future
    @Column(name = "date_to")
    private Date dateTo;
    @Valid
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private Car car;
    @Min(0)
    @Column(name = "cost")
    private Double cost;
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    Status status;

    public Booking(@NotBlank UUID bookingId, @Valid User user, @NotBlank @Future Date dateFrom,
                   @NotBlank @Future Date dateTo, @Valid Car car, @Min(0) Double cost,Status status) {
        this.bookingId = bookingId;
        this.user = user;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.car = car;
        this.cost = cost;
        this.status=status;
    }

    public Booking() {
    }

    public UUID getBookingId() {
        return bookingId;
    }

    public void setBookingId(UUID bookingId) {
        this.bookingId = bookingId;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }


    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
