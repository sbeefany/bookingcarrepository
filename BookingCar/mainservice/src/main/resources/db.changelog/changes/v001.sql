create table user_entity
(
    id          uuid         not null unique,
    email       varchar(255) not null unique,
    first_name  varchar(255) not null,
    second_name varchar(255) not null,
    primary key (id)
);
create table car
(
    id          uuid         not null unique,
    brand       varchar(255) not null,
    name        varchar(255) not null,
    rating      real         not null check (rating >= 0 and rating <= 5),
    trips_count integer      not null,
    type_car    integer      not null,
    base_price  integer      not null,
    primary key (id)
);
create table booking
(
    id        uuid             not null unique,
    cost      double precision not null,
    date_from timestamp        not null,
    date_to   timestamp        not null,
    car_id    uuid             not null,
    user_id   uuid             not null,
    status    varchar(255)     not null,

    foreign key (car_id) references car (id),
    foreign key (user_id) references user_entity (id),
    primary key (id)
);