package ru.bookingcar.service.Service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import ru.bookingcar.service.Data.Entities.Booking;
import ru.bookingcar.service.Data.Entities.Car;
import ru.bookingcar.service.Data.Enums.Status;
import ru.bookingcar.service.Data.Repository.BookingRepository;
import ru.bookingcar.service.Exeptions.CarNotFoundException;
import ru.bookingcar.service.Exeptions.UserNotFoundException;
import ru.bookingcar.service.Mappers.BookingMapper;
import ru.bookingcar.service.Mappers.DatesMapper;
import ru.bookingcar.service.controllers.DTO.Request.BookingCreationDTO;

import java.text.ParseException;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class BookingServiceImplTest {

    @Autowired
    BookingService bookingService;
    @MockBean
    private BookingRepository bookingRepository;
    @MockBean
    private BookingMapper bookingMapper;
    private DatesMapper datesMapper = new DatesMapper();

    @Test
    public void createBooking_shouldBeCreated() throws UserNotFoundException, ParseException, CarNotFoundException {
        BookingCreationDTO bookingCreationDTO = new BookingCreationDTO();
        bookingCreationDTO.setDateFrom("30/12/2022");
        bookingCreationDTO.setDateTo("30/12/2024");
        bookingCreationDTO.setCarId(UUID.randomUUID());
        bookingCreationDTO.setUserId(UUID.randomUUID());

        Mockito.doReturn(List.of())
                .when(bookingRepository)
                .findBookingsWithDates(datesMapper.convertFromString("30/12/2022"),datesMapper.convertFromString("30/12/2024"));

        bookingService.createBooking(bookingCreationDTO);

        Mockito.verify(bookingRepository).save(Mockito.any());
    }

    @Test
    public void createBooking_shouldBeNotCreated() throws UserNotFoundException, ParseException, CarNotFoundException {

        BookingCreationDTO bookingCreationDTO = new BookingCreationDTO();
        bookingCreationDTO.setDateFrom("30/12/2022");
        bookingCreationDTO.setDateTo("30/12/2024");
        bookingCreationDTO.setCarId(UUID.randomUUID());
        Car car = new Car();
        car.setCarId(bookingCreationDTO.getCarId());
        Booking booking = new Booking(null,null,datesMapper.convertFromString("30/12/2022"),datesMapper.convertFromString("30/12/2024"),
                car,55.0, Status.Open);

        Mockito.doReturn(List.of(booking))
                .when(bookingRepository)
                .findBookingsWithDates(datesMapper.convertFromString("30/12/2022"),datesMapper.convertFromString("30/12/2024"));

        Throwable throwable = assertThrows(IllegalStateException.class,()->{
            bookingService.createBooking(bookingCreationDTO);
        });
        assertNotNull(throwable.getMessage());


    }

    @Test
    public void createBooking_thereIsOneCarAndNewCarShouldBeCreated() throws UserNotFoundException, ParseException, CarNotFoundException {

        BookingCreationDTO bookingCreationDTO = new BookingCreationDTO();
        bookingCreationDTO.setDateFrom("30/12/2022");
        bookingCreationDTO.setDateTo("30/12/2024");
        bookingCreationDTO.setCarId(UUID.randomUUID());
        Car car = new Car();
        car.setCarId(UUID.randomUUID());
        Booking booking = new Booking(null,null,datesMapper.convertFromString("30/12/2022"),datesMapper.convertFromString("30/12/2024"),
                car,55.0, Status.Open);

        Mockito.doReturn(List.of(booking))
                .when(bookingRepository)
                .findBookingsWithDates(datesMapper.convertFromString("30/12/2022"),datesMapper.convertFromString("30/12/2024"));

        bookingService.createBooking(bookingCreationDTO);

        Mockito.verify(bookingRepository).save(Mockito.any());

    }
}