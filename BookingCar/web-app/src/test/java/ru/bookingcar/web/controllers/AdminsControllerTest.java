package ru.bookingcar.web.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.bookingcar.web.controllers.DTO.CarRequestDTO;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class AdminsControllerTest {

    @Autowired
    MockMvc mockMvc;


    @Test
    void addNewCar_forbidden() throws Exception {
        CarRequestDTO carRequestDTO = new CarRequestDTO();
        carRequestDTO.setTypeCar("Hatchback");
        carRequestDTO.setBasePrice(1000);
        carRequestDTO.setName("Vesta");
        carRequestDTO.setBrand("Lada");
        carRequestDTO.setTripsCount(3);

        ObjectMapper objectMapper = new ObjectMapper();
        this.mockMvc.perform(post("/cars/")
                .content(objectMapper.writeValueAsString(carRequestDTO)))
                .andExpect(status().isForbidden());
    }

}