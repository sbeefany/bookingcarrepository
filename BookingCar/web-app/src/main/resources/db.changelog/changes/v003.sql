create table user_auth(
    id uuid not null unique,
    username varchar(255) not null unique ,
    password varchar(255) not null ,
    role varchar(255) not null ,
    status varchar(255) not null ,
    email varchar(255) not null unique ,
    first_name varchar(255) not null ,
    second_name varchar(255) not null,
    primary key (id)
)