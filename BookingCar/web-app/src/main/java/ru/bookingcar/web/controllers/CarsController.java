package ru.bookingcar.web.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import ru.bookingcar.web.Exceptions.UserNotFoundException;
import ru.bookingcar.web.controllers.DTO.BookingCreationDTO;
import ru.bookingcar.web.controllers.DTO.CarResponseDTO;
import ru.bookingcar.web.controllers.DTO.UserDTO;
import ru.bookingcar.web.service.UserDetailsServiceImpl;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/v1/cars")
public class CarsController {

    private final RestTemplate restTemplate;
    private final UserDetailsServiceImpl userDetailsService;

    private UserDTO userDTO;

    @Value("${baselink}")
    private String BASE_URL;

    @Autowired
    public CarsController(RestTemplate restTemplate, UserDetailsServiceImpl userDetailsService) {
        this.userDetailsService = userDetailsService;
        this.restTemplate = restTemplate;
    }


    @GetMapping("/{id}")
    public ResponseEntity<CarResponseDTO> findCarById(@PathVariable UUID id) {

        try {
            ObjectMapper mapper = new ObjectMapper();
            ResponseEntity<String> response = restTemplate.getForEntity(BASE_URL + "/cars/" + id, String.class);
            JsonNode jsonNode = mapper.readValue(response.getBody(), JsonNode.class);
            UUID carId = UUID.fromString(jsonNode.get("id").asText());
            String typeCar = jsonNode.get("typeCar").asText();
            String name = jsonNode.get("name").asText();
            Float rating = Float.valueOf(jsonNode.get("rating").asText());
            return ResponseEntity.ok(new CarResponseDTO(carId, typeCar, name, rating));
        } catch (JsonProcessingException | RestClientException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_GATEWAY);
        }


    }

    @GetMapping("/freeCars")
    public String findAllFreeCars(@RequestParam(value = "DateFrom", required = false) Date dateFrom,
                                  @RequestParam(value = "DateTo", required = false) Date dateTo,
                                  Model model) throws UserNotFoundException {
        userDTO = userDetailsService.loadUserDTOByUserName(SecurityContextHolder.getContext().getAuthentication().getName());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String dateFromString;
        String dateToString;
        List<CarResponseDTO> responseEntity;
        try {
            if (dateFrom == null || dateTo == null) {
                dateFromString = simpleDateFormat.format(new Date(new Date().getTime() +  24 * 60 * 60 * 1000));
                dateToString = simpleDateFormat.format(new Date(new Date().getTime() + 2 * 7 * 24 * 60 * 60 * 1000));
            } else {
                dateFromString = simpleDateFormat.format(dateFrom);
                dateToString = simpleDateFormat.format(dateTo);
            }
            responseEntity = Arrays.stream(Objects.requireNonNull(restTemplate.getForObject(
                    BASE_URL + "/cars/freeCars?dateFrom=" + dateFromString +
                            ",dateTo=" + dateToString, CarResponseDTO[].class))).collect(Collectors.toList());

            BookingCreationDTO bookingCreationDTO = new BookingCreationDTO();
            bookingCreationDTO.setCarId(null);
            bookingCreationDTO.setUserId(userDTO.getId());
            bookingCreationDTO.setDateFrom(dateFromString);
            bookingCreationDTO.setDateTo(dateToString);
            model.addAttribute("carResponseDTO",new CarResponseDTO());
            model.addAttribute("userDTO", userDTO);
            model.addAttribute("carsList", responseEntity) ;
            model.addAttribute("dateFrom", dateFromString);
            model.addAttribute("dateTo", dateToString);
            model.addAttribute("bookingCreationDto",bookingCreationDTO);
            model.addAttribute("error","");
            return "carsPage";
        } catch (RestClientException e) {
            model.addAttribute("error", e.getMessage());
            return "carsPage";
        }

    }


}
