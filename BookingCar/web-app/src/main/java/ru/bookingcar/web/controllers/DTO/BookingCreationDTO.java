package ru.bookingcar.web.controllers.DTO;

import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.UUID;

@Validated
public class BookingCreationDTO {

    @NotBlank
    private UUID userId;
    @NotBlank
    private UUID carId;
    @Future
    private String dateFrom;
    @Future
    private String dateTo;

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public UUID getCarId() {
        return carId;
    }

    public void setCarId(UUID carId) {
        this.carId = carId;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }
}