package ru.bookingcar.web.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.reactive.result.view.RedirectView;
import org.springframework.web.servlet.ModelAndView;
import reactor.core.publisher.Mono;
import ru.bookingcar.web.Exceptions.UserNotFoundException;
import ru.bookingcar.web.controllers.DTO.AuthDTO;
import ru.bookingcar.web.controllers.DTO.InsideUserDTO;
import ru.bookingcar.web.controllers.DTO.UserDTO;
import ru.bookingcar.web.controllers.DTO.UserRegistrationDTO;
import ru.bookingcar.web.service.UserDetailsServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/api/v1/auth")
public class AuthController {

    private final UserDetailsServiceImpl userDetailsService;



    @Autowired
    public AuthController( UserDetailsServiceImpl userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @GetMapping("/loginpage")
    public String loginPage(Model model){
        model.addAttribute("authDto",new AuthDTO());
        model.addAttribute("error","");
        return "login";
    }

   /* @PostMapping("/login")
    public String authenticate(Model model,@ModelAttribute("authDto") AuthDTO authDTO) {
        try {
            //manager.authenticate(new UsernamePasswordAuthenticationToken(authDTO.getUsername(), authDTO.getPassword()));
            UserDTO user = userDetailsService.loadUserDTOByUserName(authDTO.getUsername());
            model.addAttribute("UserRole",user.getRole().convertToGrantedAuthorities());
            return "redirect:/api/v1/pages/mainpage";
        } catch (UserNotFoundException | AuthenticationException e) {
            model.addAttribute("error",e.getMessage());
            return "redirect:/api/v1/auth/loginpage";
        }
    }*/

    @GetMapping("/registration")
    public String registrationWindow(Model model){
        model.addAttribute("registrationDto",new UserRegistrationDTO());
        model.addAttribute("error","");
        return "registration";
    }

    @PostMapping("/registration")
    public String registration (@ModelAttribute("registrationDto") UserRegistrationDTO userRegistrationDTO,Model model) {
        try {
            userDetailsService.registration(userRegistrationDTO);
            return "redirect:/api/v1/auth/loginpage";
        }catch (WebClientResponseException  | IllegalArgumentException e){
            model.addAttribute("error",e.getMessage());
            return "registration";
        }

    }

    @PostMapping("/logout")
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        SecurityContextLogoutHandler securityContextLogoutHandler = new SecurityContextLogoutHandler();
        securityContextLogoutHandler.logout(request, response, null);
    }
}
