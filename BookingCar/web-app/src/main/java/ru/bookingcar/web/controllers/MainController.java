package ru.bookingcar.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.bookingcar.web.Entities.User;
import ru.bookingcar.web.Exceptions.UserNotFoundException;
import ru.bookingcar.web.controllers.DTO.UserDTO;
import ru.bookingcar.web.service.UserDetailsServiceImpl;

@Controller
@RequestMapping("/api/v1/pages")
public class MainController {

    private final UserDetailsServiceImpl userDetailsService;

    @Autowired
    public MainController(UserDetailsServiceImpl userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @GetMapping("/mainpage")
    public String getMainWindow(Model model)  {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            UserDTO userDTO = userDetailsService.loadUserDTOByUserName(username);
            model.addAttribute("userDTO",userDTO);
        } catch (UserNotFoundException e) {
            e.printStackTrace();
        }

        return "mainWindow";
    }
}
