package ru.bookingcar.web.controllers.DTO;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.UUID;

public class CarResponseDTO {

    @NotBlank
    private UUID id;

    @NotBlank
    private String typeCar;

    @NotBlank
    private String name;

    private String brand;

    @Min(0)
    @Max(5)
    private float rating;

    public CarResponseDTO() {
    }

    public CarResponseDTO(@NotBlank UUID id, @NotBlank String typeCar, @NotBlank String name, @Min(0) @Max(5) float rating) {
        this.id = id;
        this.typeCar = typeCar;
        this.name = name;
        this.rating = rating;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getTypeCar() {
        return typeCar;
    }

    public void setTypeCar(String typeCar) {
        this.typeCar = typeCar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
