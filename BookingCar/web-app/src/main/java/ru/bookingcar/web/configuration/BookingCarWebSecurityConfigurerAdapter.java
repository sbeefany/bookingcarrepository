package ru.bookingcar.web.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import ru.bookingcar.web.Entities.enums.Role;
//import ru.bookingcar.web.JWT.JwtConfigure;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class BookingCarWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

    //private final JwtConfigure jwtConfigurer;
    private final PasswordEncoder passwordEncoder;
    private final UserDetailsService userDetailsService;

    public BookingCarWebSecurityConfigurerAdapter( PasswordEncoder passwordEncoder,@Qualifier("userDetailsServiceImpl") UserDetailsService userDetailsService) {
        //this.jwtConfigurer = jwtConfigurer;
        this.passwordEncoder = passwordEncoder;
        this.userDetailsService = userDetailsService;
    }


    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
               // .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
               // .and()
                .authorizeRequests()
                .antMatchers("/api/v1/cars/*").authenticated()
                .antMatchers("/api/v1/bookings/*").authenticated()
                .antMatchers("/api/v1/pages/*").authenticated()
                .antMatchers("/api/v1/auth/*").permitAll()
                .antMatchers("/api/v1/admins/*").hasAuthority(Role.Admin.getName())
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/api/v1/auth/loginpage").permitAll()
                .defaultSuccessUrl("/api/v1/cars/freeCars")
                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/api/v1/auth/logout","POST"))
                .invalidateHttpSession(true)
                .clearAuthentication(true)
                .deleteCookies("JSESSIONID")
                .logoutSuccessUrl("/api/v1/auth/loginpage");
                //.and()
                //.apply(jwtConfigurer);
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(daoAuthenticationProvider());
    }

    protected DaoAuthenticationProvider daoAuthenticationProvider(){
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder);
        daoAuthenticationProvider.setUserDetailsService(userDetailsService);
        return daoAuthenticationProvider;
    }

}
