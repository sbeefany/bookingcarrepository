package ru.bookingcar.web.controllers;

import io.swagger.annotations.ApiOperation;
import org.dom4j.rule.Mode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import ru.bookingcar.web.Entities.User;
import ru.bookingcar.web.Entities.enums.Status;
import ru.bookingcar.web.Exceptions.UserNotFoundException;
import ru.bookingcar.web.controllers.DTO.*;
import ru.bookingcar.web.service.UserDetailsServiceImpl;

import javax.validation.constraints.Future;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/v1/admins")
public class AdminsController {


    private final WebClient webClient;
    private final UserDetailsServiceImpl userDetailsService;
    private final RestTemplate restTemplate;
    @Value("${baselink}")
    private String BASE_URL;

    @Autowired
    public AdminsController(WebClient webClient, UserDetailsServiceImpl userDetailsService, RestTemplate restTemplate) {
        this.webClient = webClient;
        this.userDetailsService = userDetailsService;
        this.restTemplate = restTemplate;
    }

    @GetMapping("/users/")
    private String getAllUsers(Model model) {
        try {
            UserDTO userDTO = userDetailsService.loadUserDTOByUserName(SecurityContextHolder.getContext().getAuthentication().getName());
            List<UserDTO> users = userDetailsService.getAllUsers();
            model.addAttribute("userDTO",userDTO);
            model.addAttribute("usersList",users);
            model.addAttribute("error","");
        } catch (UserNotFoundException e) {
            model.addAttribute("error",e.getMessage());
        }
        return "usersPage";
    }

    @PostMapping("/users/{id}/status")
    public ResponseEntity changeUserStatus(@PathVariable("id") UUID userId, @RequestBody Status status) {
        try {
            userDetailsService.changeStatus(userId, status);
            return ResponseEntity.ok("");
        } catch (WebClientResponseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }

    }

    @PostMapping("/cars")
    public String addNewCar(@ModelAttribute("carRequestDto") CarRequestDTO carDTO, Model model) {
        try {
            restTemplate.postForEntity(BASE_URL + "/cars/", carDTO, String.class);
            return "redirect:/api/v1/cars/freeCars";
        } catch (RestClientException e) {
            model.addAttribute("error", e.getMessage());
            return "carCreationPage";
        }

    }

    @GetMapping("/bookings")
    @ApiOperation("Метод для поиска всех бронирований на заданные даты. Метод расчитывается для работы администратора")
    public String findBookings(@RequestParam(required = false) @Future Date dateFrom,
                               @RequestParam(required = false) @Future Date dateTo,
                               Model model) {
        try {
            UserDTO userDTO = userDetailsService.loadUserDTOByUserName(SecurityContextHolder.getContext().getAuthentication().getName());
            List<BookingDTO> bookingDTOS = Arrays.stream(webClient.get()
                    .uri(uriBuilder -> uriBuilder
                            .path("/bookings/")
                            .queryParam("dateFrom", dateFrom)
                            .queryParam("dateTo", dateTo)
                            .build())
                    .retrieve()
                    .bodyToMono(BookingDTO[].class)
                    .block()).collect(Collectors.toList());
            model.addAttribute("bookingsList", bookingDTOS);
            model.addAttribute("userDTO", userDTO);
            model.addAttribute("error", "");

        } catch (WebClientResponseException | UserNotFoundException e) {
            model.addAttribute("error", e.getMessage());
        }
        return "bookingsPage";

    }

    @GetMapping("bookings/cars/{carId}")
    @ApiOperation("Метод для поиска бронирований по id машины")
    public ResponseEntity<List<BookingDTO>> findBookingByCarId(@PathVariable("carId") UUID carId) {
        try {
            return ResponseEntity.ok(webClient.get()
                    .uri(uriBuilder -> uriBuilder
                            .path("/bookings/cars/{carId}/")
                            .build(carId))
                    .retrieve()
                    .bodyToMono(List.class)
                    .block());
        } catch (WebClientResponseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @GetMapping("/cars")
    public String findAllCars(Model model) {
        try {
            List<CarResponseDTO> responseEntity = Arrays.stream(restTemplate.getForObject(BASE_URL + "/cars/", CarResponseDTO[].class)).collect(Collectors.toList());
            UserDTO userDTO = userDetailsService.loadUserDTOByUserName(SecurityContextHolder.getContext().getAuthentication().getName());
            model.addAttribute("userDTO", userDTO);
            model.addAttribute("carsList", responseEntity);
            model.addAttribute("error", "");
            return "carsPage";
        } catch (RestClientException | UserNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "carsPage";
        }

    }

    @GetMapping("/cars/creation")
    public String carCreationPage(Model model) {
        try {
            UserDTO userDTO = userDetailsService.loadUserDTOByUserName(SecurityContextHolder.getContext().getAuthentication().getName());

            model.addAttribute("userDTO", userDTO);
            model.addAttribute("carRequestDto", new CarRequestDTO());
            model.addAttribute("error", "");
        } catch (UserNotFoundException e) {
            model.addAttribute("error", e.getMessage());
        }
        return "carCreationPage";
    }
}
