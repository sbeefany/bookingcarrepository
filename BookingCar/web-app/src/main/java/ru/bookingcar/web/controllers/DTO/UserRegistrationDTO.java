package ru.bookingcar.web.controllers.DTO;

import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;
import ru.bookingcar.web.Entities.enums.Role;
import ru.bookingcar.web.Entities.enums.Status;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.UUID;
@Validated
public class UserRegistrationDTO {

    @NotBlank
    private String email;
    @NotBlank
    private String username;
    @NotBlank
    @Size(min = 5)
    private String password;
    @NotBlank
    private String firstName;

    @NotBlank
    private String secondName;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }
}
