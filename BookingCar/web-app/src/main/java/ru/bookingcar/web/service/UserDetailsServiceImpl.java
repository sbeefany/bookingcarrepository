package ru.bookingcar.web.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import ru.bookingcar.web.Entities.User;
import ru.bookingcar.web.Exceptions.UserNotFoundException;

import ru.bookingcar.web.Mappers.UserMapper;
import ru.bookingcar.web.controllers.DTO.UserDTO;
import ru.bookingcar.web.controllers.DTO.UserDetailsDTO;
import ru.bookingcar.web.Repositories.UsersRepository;
import ru.bookingcar.web.Entities.enums.Status;
import ru.bookingcar.web.controllers.DTO.UserRegistrationDTO;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service("userDetailsServiceImpl")
public class UserDetailsServiceImpl implements UserDetailsService {


    private final UsersRepository usersRepository;
    private final UserMapper userMapper;
    private final WebClient webClient;
    @Autowired
    public UserDetailsServiceImpl(
            UsersRepository usersRepository, UserMapper userMapper,
            WebClient webClient) {

        this.usersRepository = usersRepository;
        this.userMapper = userMapper;
        this.webClient = webClient;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = usersRepository.findUserByUsername(username).orElseThrow(() -> new UsernameNotFoundException("User doesn't exist"));
        return new UserDetailsDTO(user.getUsername(), user.getPassword(), user.getEmail(), Status.Active.equals(user.getStatus()), user.getRoles().convertToGrantedAuthorities());
    }


    public UserDTO loadUserDTOByUserName(String username) throws UserNotFoundException {
        User user = usersRepository.findUserByUsername(username).orElseThrow(() -> new UserNotFoundException("User doesn't exist"));
        UserDTO userDTO = userMapper.convertFromEntityToUserDTO(user);
        return userDTO;
    }

    public UserDTO loadUserDTOByUserId(UUID userId) throws UserNotFoundException {
        User user = usersRepository.findById(userId).orElseThrow(() -> new UserNotFoundException("User doesn't exist"));
        return userMapper.convertFromEntityToUserDTO(user);
    }


    public void registration(UserRegistrationDTO registrationDTO) throws IllegalArgumentException {
        User user = userMapper.convertFromRegistrationDTO(registrationDTO);
        webClient.
                post()
                .uri("/users/")
                .bodyValue(userMapper.convertFromEntity(user))
                .retrieve()
                .bodyToMono(String.class)
                .doOnSuccess(response -> {
                    usersRepository.save(user);

                }).block();
    }

    public void changeStatus(UUID userId, Status status) throws IllegalArgumentException {
        User user = usersRepository.getById(userId);
        WebClient.ResponseSpec currentWebClient;
        if (status.equals(Status.Banned)) {
            currentWebClient = webClient.delete()
                    .uri(uriBuilder -> uriBuilder
                            .path("/users/{id}/")
                            .build(userId))
                    .retrieve();
        } else {
            currentWebClient = webClient.post()
                    .uri("/users/")
                    .bodyValue(userMapper.convertFromEntity(user))
                    .retrieve();
        }
        currentWebClient
                .bodyToMono(String.class)
                .doOnSuccess(response -> {
                    usersRepository.save(user);
                })
                .block();
    }

    public List<UserDTO> getAllUsers() {
        return usersRepository.findAll().stream().map(userMapper::convertFromEntityToUserDTO).collect(Collectors.toList());
    }

    public UUID getInsideUUID(UUID userId) throws UserNotFoundException, JsonProcessingException {
        User user = usersRepository.findById(userId).orElseThrow(() -> new UserNotFoundException("User doesn't exist"));
        String insideUUIDString = webClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path("/users/{email}")
                        .build(user.getEmail()))
                .retrieve()
                .bodyToMono(String.class)
                .block();

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode=objectMapper.readValue(insideUUIDString, JsonNode.class);
        return UUID.fromString(jsonNode.get("id").asText());
    }
}
