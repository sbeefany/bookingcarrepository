package ru.bookingcar.web.Entities.enums;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

public enum Role {
    User(Set.of(Permission.Read),"User"),Admin(Set.of(Permission.Read,Permission.Write,Permission.Delete),"Admin");

    private final Set<Permission> permissions;
    private final String name;

    Role(Set<Permission> permissions,String name) {
        this.permissions = permissions;
        this.name = name;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    public Set<SimpleGrantedAuthority> convertToGrantedAuthorities(){
        return Set.of(new SimpleGrantedAuthority(this.getName()));
    }

    public String getName() {
        return name;
    }
}


