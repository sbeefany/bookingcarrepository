package ru.bookingcar.web.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.bookingcar.web.Entities.User;

import java.util.Optional;
import java.util.UUID;

public interface UsersRepository extends JpaRepository<User, UUID> {

    Optional<User> findUserByUsername(String username);
}
