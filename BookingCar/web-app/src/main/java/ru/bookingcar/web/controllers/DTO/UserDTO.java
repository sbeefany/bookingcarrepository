package ru.bookingcar.web.controllers.DTO;

import ru.bookingcar.web.Entities.enums.Role;
import ru.bookingcar.web.Entities.enums.Status;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

public class UserDTO {

    private UUID id;
    @NotBlank
    private String email;
    @NotBlank
    private String username;
    private Status status;
    private Role role;

    public UserDTO(UUID id,String email, String username, Status status, Role role) {
        this.id = id;
        this.username = username;
        this.status = status;
        this.role = role;
        this.email = email;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
