package ru.bookingcar.web.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import ru.bookingcar.web.Exceptions.UserNotFoundException;
import ru.bookingcar.web.controllers.DTO.BookingCreationDTO;
import ru.bookingcar.web.controllers.DTO.BookingDTO;
import ru.bookingcar.web.controllers.DTO.UserDTO;
import ru.bookingcar.web.service.UserDetailsServiceImpl;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/v1/bookings")
public class BookingsController {

    private final WebClient webClient;
    private final UserDetailsServiceImpl userDetailsService;

    @Autowired
    public BookingsController(WebClient webClient, UserDetailsServiceImpl userDetailsService) {
        this.webClient = webClient;
        this.userDetailsService = userDetailsService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<BookingDTO> findBookingById(@PathVariable("id") UUID bookingId) {
        try {
            return ResponseEntity.ok(webClient.get()
                    .uri(uriBuilder -> uriBuilder
                            .path("/bookings/{bookingid}/")
                            .build(bookingId))
                    .retrieve()
                    .bodyToMono(BookingDTO.class)
                    .block());
        } catch (WebClientResponseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }

    @PostMapping("/")
    public String createBooking(@ModelAttribute("bookingCreationDto") BookingCreationDTO bookingCreationDTO, Model model) {

        try {
            UUID firstID = bookingCreationDTO.getUserId();
            UUID uuid = userDetailsService.getInsideUUID(bookingCreationDTO.getUserId());
            bookingCreationDTO.setUserId(uuid);
             webClient.post()
                    .uri(uriBuilder -> uriBuilder
                            .path("/bookings/newBooking")
                            .build())
                    .bodyValue(bookingCreationDTO)
                    .retrieve()
                    .bodyToMono(BookingDTO.class)
                    .block();
            return "redirect:/api/v1/bookings/users/"+firstID;
        } catch (JsonProcessingException | UserNotFoundException | WebClientResponseException e) {
            model.addAttribute("error",e.getMessage());
            return "redirect:/api/v1/cars/freeCars";
        }
    }

    @GetMapping("/users/{userId}")
    @ApiOperation("Метод для поиска бронирований по id пользователя")
    public String findBookingByUserId(@PathVariable("userId") UUID userId,Model model) {
        try {
            UserDTO userDTO = userDetailsService.loadUserDTOByUserId(userId);
            UUID uuid = userDetailsService.getInsideUUID(userId);
            List<BookingDTO> bookingDTOS= Arrays.stream(webClient.get()
                    .uri(uriBuilder -> uriBuilder
                            .path("/bookings/users/{userId}/")
                            .build(uuid))
                    .retrieve()
                    .bodyToMono(BookingDTO[].class)
                    .block())
                    .collect(Collectors.toList());
            model.addAttribute("bookingsList",bookingDTOS);
            model.addAttribute("userDTO",userDTO);
            model.addAttribute("error","");
            return "bookingsPage";
        } catch (JsonProcessingException | UserNotFoundException | WebClientResponseException e) {
            model.addAttribute("error",e.getMessage());
            return "bookingsPage";
        }
    }

}
