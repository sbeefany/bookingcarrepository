package ru.bookingcar.web.Mappers;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.bookingcar.web.Entities.User;
import ru.bookingcar.web.Entities.enums.Role;
import ru.bookingcar.web.Entities.enums.Status;

import ru.bookingcar.web.controllers.DTO.InsideUserDTO;
import ru.bookingcar.web.controllers.DTO.UserDTO;
import ru.bookingcar.web.controllers.DTO.UserRegistrationDTO;

import java.util.UUID;

@Component
public class UserMapper {


    private final PasswordEncoder passwordEncoder;

    public UserMapper( PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public  User convertFromRegistrationDTO(UserRegistrationDTO registrationDTO){
        return new User(UUID.randomUUID(),registrationDTO.getUsername(),
                registrationDTO.getEmail(),registrationDTO.getFirstName(),registrationDTO.getSecondName(),
                passwordEncoder.encode(registrationDTO.getPassword()), Role.User, Status.Active);
    }

    public InsideUserDTO convertFromEntity(User user){
        InsideUserDTO newUserDto = new InsideUserDTO();
        newUserDto.setEmail(user.getEmail());
        newUserDto.setFirstName(user.getFirstName());
        newUserDto.setSecondName(user.getSecondName());
        return newUserDto;
    }

    public UserDTO convertFromEntityToUserDTO(User user){
        return new UserDTO(user.getId(),user.getEmail(),user.getUsername(),user.getStatus(),user.getRoles());
    }
}
