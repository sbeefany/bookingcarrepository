package ru.bookingcar.web.Entities.enums;

public enum Permission {
    Read("read"),Write("write"),Delete("delete");

    private final String permission;

    Permission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }
}
